# REPTE: Anàlisi dels drets sobre dades personals

> ATENCIÓ: Els noms dels equips hosts al VirtualBox, el nom del sistema quan instal·leu, els noms de l'usuari, equip, etc. han de ser el vostre per a que a les captures es pugui veure.

> Teniu l'exercici a fer al final d'aquest enunciat. Llegiu primer tot el document abans de començar la pràctica.

## Referències i ajuda

- Agència de protecció de dades - https://www.aepd.es/

## Objectius

- L'objectiu d'aquesta activitat és conèixer quines dades personals guarden les xarxes socials i els proveïdors de serveis com ara Google. També conèixer com ens pot ajudar l'AEPD en l'exercici dels nostres drets.

## Introducció

- La LOPDGDD i el RGPD obliguen a qualsevol entitat que treballi amb dades personals de persones físiques a:
  - Informar sobre els drets que tenen els interessats sobre les dades personals cedides.
  - Informar de com es poden exercir els drets esmentats.
  - Explicar altres aspectes relacionats amb les dades personals.

## Exercici

1. Busca a les xarxes socials on tinguis compte (mínim Google i Instagram o una altra) com s'accedeix a les dades personals per exercir el dret d'accés i portabilitat. Explica-ho amb les teves paraules.

2. Fes un informe amb captures sobre quines dades s'estan registrant i explica amb les teves paraules quina utilitat poden tenir. Explica quina mida tenen les dades que es guarden.

3. Busca als documents de privacitat i termes legals els apartats on explica com exercir els drets de l'interessat. Explica-ho amb les teves paraules.

4. Explica amb les teves paraules quins termes i condicions trobes més impactants en aquests documents legals (com ara potser cedir els drets sobre les imatges que hi posis).

5. Busca a l'agència de protecció de dades https://www.aepd.es/ quines són les seves funcions o com pot ajudar els ciutadans i explica-ho amb les teves paraules. NO AMB CAPTURES NI CUT & PASTE

6. Explica com ens pot ajudar l'AEPD en el cas concret d'exercir els drets que et reconeix la LOPDGDD.
