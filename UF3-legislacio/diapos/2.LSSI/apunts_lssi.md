# LSSI (LLEI DE SERVEIS DE LA SOCIETAT DE LA INFORMACIÓ I DEL COMERÇ ELECTRÒNIC):

**INS Carles Vallbona**

**Pau Tomé**

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [LSSI (LLEI DE SERVEIS DE LA SOCIETAT DE LA INFORMACIÓ I DEL COMERÇ ELECTRÒNIC):](#lssi-llei-de-serveis-de-la-societat-de-la-informaci-i-del-comer-electrnic)
	- [PER QUE SERVEIX AQUESTA LLEI?](#per-que-serveix-aquesta-llei)
	- [QUE ES UN PRESTADOR DE SERVEIS](#que-es-un-prestador-de-serveis)
	- [QUI ESTA SUBJECTE A LA LSSI?](#qui-esta-subjecte-a-la-lssi)
	- [QUE CAL PER PRESTAR SERVEIS A INTERNET?](#que-cal-per-prestar-serveis-a-internet)
	- [OBLIGACIO D'INFORMACIO](#obligacio-dinformacio)
		- [EMPRESES I CIUTADANS](#empreses-i-ciutadans)
		- [EMPRESES](#empreses)
	- [QUE HAN DE FER ELS PROVEIDORS DE SERVEIS?](#que-han-de-fer-els-proveidors-de-serveis)
		- [OBLIGACIONS ESPECIFIQUES DELS PROVEIDORS DE SERVEIS](#obligacions-especifiques-dels-proveidors-de-serveis)
		- [RESPONSABILITAT LEGAL DELS PROVEIDORS DE SERVEIS](#responsabilitat-legal-dels-proveidors-de-serveis)
	- [LA PUBLICITAT ELECTRÒNICA](#la-publicitat-electrnica)
	- [QUE PASSA SI S’INCOMPLEIX LA LSSI-CE?](#que-passa-si-sincompleix-la-lssi-ce)
	- [LLEUS (EXEMPLES):](#lleus-exemples)
	- [GREUS (EXEMPLES):](#greus-exemples)
	- [MOLT GREUS (EXEMPLES):](#molt-greus-exemples)
	- [FONTS](#fonts)

<!-- /TOC -->

## PER QUE SERVEIX AQUESTA LLEI?

- Vídeo LSSI, adapta la teva web (resum molt ràpid publicitari) - https://www.youtube.com/watch?v=2Zig5kLBliM
- Veure explicació de tot a la Web LSSI-CE Govern Espanya - http://www.lssi.gob.es/paginas/Index.aspx

---

1. Regula l'activitat de les organitzacions que treballen en la societat de la Informació

- Ex: Compra i venda, publicitat, subhastes, contractació, etc.

- Es posen regles per evitar els enganys als usuaris que volen comprar via internet.

2. Regula el comportament dels prestadors de serveis.

3. Defineix les regles de la publicitat electrònica.

4. Estableix com ha de ser la contractació electrònica.

5. S’estableix la validesa dels contractes on-line (tenen la mateixa validesa que els de paper)


---

## QUE ES UN PRESTADOR DE SERVEIS

- És algú que obté un benefici econòmic de fer un servei a un usuari.
- Els prestadors de serveis inclouen:
  - **Proveïdors de serveis**: Operadors de xarxa i serveis de comunicacions electròniques, inclosos els ISP (Internet Service Provider) que donen accès a Internet
  - **Empreses** que ofereixen serveis (normalment comerç electrònic).
  - **Ciutadans** que tinguin la seva pròpia web.

---

## QUI ESTA SUBJECTE A LA LSSI?

1. Queden fora d'aquesta llei els següents mitjans electrònics, que tenen lleis pròpies:
- Ràdio.
- Televisió.
- Teletext
- Correu electrònic no-comercial.

2. Obliga a qualsevol que faci algun tipus d'activitat econòmica per xarxes de telecomunicacions
  - Sempre que tingui alguna **seu permanent** a l'Estat Espanyol.

3. Qualsevol cosa que comporti ingressos: DIRECTES o INDIRECTES
   - La **publicitat** es considera una activitat comercial.
   - La **promoció** es considera una activitat comercial.

---

## QUE CAL PER PRESTAR SERVEIS A INTERNET?

- **No cal cap autorització especial** per poder prestar serveis a la Xarxa
- La llei vol protegir els consumidors
- Obliga a oferir informació permanent, fàcil i gratuïta sobre la empresa venedora

---

## OBLIGACIO D'INFORMACIO

### EMPRESES I CIUTADANS

El prestador de serveis **ha de proporcionar a la seva web** al menys (Tant empreses com persones físiques)
  - Nom de l'empresa o nom social
  - Adreça
  - NIF/CIF
  - Forma de contacte (correu electrònic, telèfon o fax)

  - Donar les dades que permetin saber com són els productes i el que valen exactament.

  - Si es fan **contractes electrònics**:
    1. Cal fer constar els tràmits o passes per celebrar el contracte (la venda, per exemple).

    2. Pases que ha de fer l'usuari per completar la compra.

    3. Indicar si el prestador arxiva el document electrònic del contracte i si aquest serà accessible.

    4. Mitjans tècnics per identificar i corregir errors

    5. LLengües en que es formalitza el contracte

    6. Posar a disposició de l'usuari les condicions generals del contracte

    7. I després de fer el contracte, obligació de confirmar l'acceptació del contracte, normalment amb un correu de confirmació de la comanda.

---

### EMPRESES

**En el cas d'empreses també està obligat a:**

- Dades d'inscripció registral (registre mercantil o d'altres)

- Codis de conducta als que estigui adherida

- Si l’activitat **requereix autorització**, informació sobre com s’ha obtingut i on.
  - Exemples: Agències de viatges, serveis de lloguer turístics, armeries, centres i serveis sanitaris, etc.

- Si l'**activitat és regulada** cal indicar les dades del títol acadèmic i el col·legi professional.
  - Exemples: Dentista, Farmacèutic, Enginyer, Arquitecte ...

---

## QUE HAN DE FER ELS PROVEIDORS DE SERVEIS?

- Els proveïdors de serveis d'intermediació poden ser:
  - Empreses que donen connexió a Internet als clients (ISP).
  - Prestadors de serveis d'allotjament  de dades.
  - Buscadors i proveïdors d'enllaços.

### OBLIGACIONS ESPECIFIQUES DELS PROVEIDORS DE SERVEIS

1. Col·laborar amb òrgans públics per executar resolucions que no poden complir-se sense la seva ajuda.
2. Informar als seus clients sobre els mitjans tècnics que millorin la seguretat de la informació (anti-virus, anti-programes espia, filtres de correu)
  - Els aplicats per ells
  - Les eines existents pel filtrat i restricció d'acceès a determinats continguts i serveis
  - Les possibles responsabilitats dels usuarios si fan ús d'internet amb finalitats il·lícites.

### RESPONSABILITAT LEGAL DELS PROVEIDORS DE SERVEIS

- Els continguts **són responsabilitat** de qui els genera, no del proveïdor.

- Els proveïdors només són responsables dels continguts si...
  - Saben que el contingut és il.lícit i no l'eliminen.
  - Les autoritats els ordenen que l'eliminin i no ho fan.

---

## LA PUBLICITAT ELECTRÒNICA

La publicitat s'ha de poder distingir clarament:
 - El missatge ha de ser clar i no portar a equivocs.
 - Si són promocions, concursos, etc. ha de quedar clar..

Queda **prohibit el correu electrònic comercial sense el consentiment** del receptor

---

## QUE PASSA SI S’INCOMPLEIX LA LSSI-CE?

Les **agències de protecció de dades** poden multar incompliments de la **LSSI-CE**

Les agències de protecció de dades tenen **màxima autoritat**

Hi ha tres tipus de sancions:

- Lleus.
- Greus.
- Molt greus.

---

## LLEUS (EXEMPLES):
**Fins a 30.000€**

- No informar de forma fàcil, gratuïta, permanent de les dades.
- Enviar correu comercial sense identificar-se.
- No confirmar les comandes.

## GREUS (EXEMPLES):
**de 30.001€ a 150.000€**

- Enviament massiu de correu comercial.
- Incomplir el deure d'informació general.
- No confirmar les comandes reiteradament.
- No proporcionar les condicions del contracte
- Resistir-se a una inspecció dels òrgans facultats.

## MOLT GREUS (EXEMPLES):
**de 150.001€ a 600.000€ i la prohibició d'actuar a Espanya**

- Incomplir les ordres dels òrgans competents per retirar o aturar continguts.

---

## FONTS

- Web LSSI-CE Govern Espanya - http://www.lssi.gob.es/paginas/Index.aspx
- Compliment LSSI en Web - http://www.emfasi.com/ca/glosario/la-llei-sobre-serveis-de-la-societat-de-la-informacio-i-de-comerc-electronic
