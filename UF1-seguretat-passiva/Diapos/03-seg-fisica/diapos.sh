# -s standalone file (un sol html i no un fragment)
# -i mostrar llistes de forma incremental (item a item)
# -t format de sortida
# --webtex convertir fòrmules Latex a imatge .png
# --mathml convertir fòrmules Latex a un format mathml que cal que el navegador suporti
# --mathjax embebir fòrmules Latex a la pàgina directament
# --standalone html amb totes les imatges, etc en un sol fitxer

echo Paràmetres: 1-fitxer origen

# pandoc -s --standalone --mathml -t dzslides $1 -o a.html
pandoc -s --standalone --webtex -t slidy $1 -o $1.html
# pandoc -s --standalone --mathjax -t revealjs $1 -o c.html
